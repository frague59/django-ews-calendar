from django.apps import AppConfig


class DjangoEwsCalendarConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'django_ews_calendar'
